/**
 *======================================================================================
 * MeasuresController.java
 *======================================================================================
 *
 *	Definitions for class:
 *		- MeasuresController
 *
 *======================================================================================
 *	Modification History:
 *======================================================================================
 *
 *  Date:			01/25/2016
 *  				Original development
 *  @author 		Jayesh Bharati
 *  @version 		1.0
 *  Description:  	MeasuresController is a RESTful controller.
 *  				This controller is used for 
 *  				1. To Retrieve Patient Measures Data 
 *					2. To Update Patient Measures Data.
 *  				Design Pattern(s):
 *  				 - DAO
 *  				 
 *  
 *======================================================================================
 *	Copyright 2016, Sandlot Solutions. All rights reserved.
 *======================================================================================
 **/

// Package 
package com.sandlotsolutions.MetrixServer.Controller;

// System Imports
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

// Logging imports
import org.apache.log4j.Logger;

// Project Imports
import com.sandlotsolutions.MetrixServer.DAO.MeasuresDAO;
import com.sandlotsolutions.MetrixServer.Model.Request;
import com.sandlotsolutions.MetrixServer.Model.Response;
import com.sandlotsolutions.MetrixServer.Util.CommonUtil;
import com.sandlotsolutions.MetrixServer.Util.MeasuresURIEndPoints;

@RestController
public class MeasuresController {
	
	private static final Logger log = Logger.getLogger(MeasuresController.class);

	@Autowired
	public MeasuresDAO measuresDAO;


	//===================================================================
	// PUBLIC FUNCTIONS
	//===================================================================
	/** 
	 *
	 * This method retrieves patient's measures details.
	 * 
	 * @param request      request param holds the measures request parameters
	 * 
	 * @preconditions			request contains following mandatory parameters
	 * 							- patientId
	 * 							- providerId
	 * 							- patientNameSpaceId 
	 *
	 * @return Response 	- This object holds patient's measures details and action taken on these measures.
	 * 
	 **/
	@RequestMapping(value = MeasuresURIEndPoints.GET_MEASURES, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,headers = "Accept=application/json")
	public ResponseEntity<Response> getMeasures(@RequestBody Request request) throws Exception{
		
		log.debug("User Logged in : "+request.getUser());
		log.debug("Started fetching measures details for "+request.getPatientId());
		
		Response measuresResponse = new Response();
		
		//Check NuLL or Empty values for mandatory request parameters
		if(checkNullOrEmpty(request)){
			String errorMsg = "Please check mandatory request parameter.[PatienId/ProviderId/NamespaceId]should not be EMPTY or NULL ";
			log.error(errorMsg);
			measuresResponse = buildErrorResponse(errorMsg);
			return new ResponseEntity<Response>(measuresResponse, HttpStatus.PRECONDITION_FAILED);
		}
		
		try {

			measuresResponse = measuresDAO.getMeasures(request);
		} catch (SQLException se) {

			log.error("SQL error", se);
			measuresResponse = buildErrorResponse(se.getMessage());
		} catch (Exception ex) {

			log.error("Application error", ex);
			measuresResponse = buildErrorResponse(ex.getMessage());
		}

		return new ResponseEntity<Response>(measuresResponse,HttpStatus.OK);
	}
	
	/**
	 * This is a private method to check NULL and empty parameters from request
	 * */
	private boolean checkNullOrEmpty(Request request){
		boolean flagNullOrEmpty = CommonUtil.verifyNullOrEmpty(request.getPatientId(), request.getProviderId(),
				request.getPatientNameSpaceId());
		
		return flagNullOrEmpty;
	}
	
	
		 
	 private Response buildErrorResponse(String errorMessage){
	    	Response response = new Response();
	    	response.setStatus("Fail");
	    	response.setErrorMessage(errorMessage);
	    	return response;
	    }

	
}
