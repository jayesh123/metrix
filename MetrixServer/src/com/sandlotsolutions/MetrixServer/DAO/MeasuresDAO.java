/**
 *======================================================================================
 * MeasuresDAO.java
 *======================================================================================
 *
 *	Definitions for class:
 *		- MeasuresDAO
 *
 *======================================================================================
 *	Modification History:
 *======================================================================================
 *
 *  Date:			01/25/2016
 *  				Original development
 *  @author 		Jayesh Bharati
 *  @version 		1.0
 *  Description:  	MeasuresDAO is a interface.
 *  
 *======================================================================================
 *	Copyright 2016, Sandlot Solutions. All rights reserved.
 *======================================================================================
 **/

// Package
package com.sandlotsolutions.MetrixServer.DAO;

import java.sql.SQLException;
import java.text.ParseException;

import com.sandlotsolutions.MetrixServer.Model.Request;
import com.sandlotsolutions.MetrixServer.Model.Response;

public interface MeasuresDAO {
	public Response getMeasures(Request request) throws SQLException, ParseException;
}
