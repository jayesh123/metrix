/**
 *======================================================================================
 * MeasuresDAOImpl.java
 *======================================================================================
 *
 *	Definitions for class:
 *		- MeasuresDAOImpl
 *
 *======================================================================================
 *	Modification History:
 *======================================================================================
 *
 *  Date:			01/25/2016
 *  				Original development
 *  @author 		Jayesh Bharati
 *  @version 		1.0
 *  Description:  	MeasuresDAOImpl is a DAO implementation class.
 *  				This class performs database operations.
 *  
 *======================================================================================
 *	Copyright 2016, Sandlot Solutions. All rights reserved.
 *======================================================================================
 **/

// Package
package com.sandlotsolutions.MetrixServer.DAO;

// System Imports
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.sandlotsolutions.MetrixServer.Model.ActionTakenModel;
import com.sandlotsolutions.MetrixServer.Model.MeasuresModel;
import com.sandlotsolutions.MetrixServer.Model.Request;
import com.sandlotsolutions.MetrixServer.Model.Response;
import com.sandlotsolutions.MetrixServer.Util.CommonUtil;
import com.sandlotsolutions.MetrixServer.Util.SQLQueryConstants;

@Repository
public class MeasuresDAOImpl implements MeasuresDAO {
	private static final Logger log = Logger.getLogger(MeasuresDAOImpl.class);
	
	@Autowired
	private DataSource dataSource;
	
	//===================================================================
	// PUBLIC FUNCTIONS
	//===================================================================
	/** 
	 *
	 * This is a overridden method from MeasuresDAO interface.
	 * It retrieves patients's measures details from database.
	 * 
	 * @param request      request param holds the measures request parameters
	 * 
	 * @return Response 	Holds Patient's measures and its action taken details
	 * @throws SQLException 
	 * @throws ParseException 
	 * 
	 **/
	@Override
	public Response getMeasures(Request request) throws SQLException{
		
		log.debug("getMeasures : Started fetching Measures details");
		
		Response measuresResponse = new Response();
		
		List<MeasuresModel> measureDetails = new ArrayList();

		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		ResultSet rs = null;
		try {
			dbConnection = dataSource.getConnection();
			preparedStatement = dbConnection.prepareStatement(SQLQueryConstants.GET_MEASURES_SQL);
			preparedStatement.setString(1, request.getPatientId());
			rs = preparedStatement.executeQuery();

			while (rs.next()) {

				// set Measures details
				MeasuresModel measuresDetails = new MeasuresModel();
				measuresDetails.setMeasureName(rs.getString("measure name").trim());
				measuresDetails.setMeasureCode(rs.getString("measure code").trim());
				measuresDetails.setMeasureDescription(rs.getString("measure description").trim());
				measuresDetails.setMeasureSpecificationCode(rs.getString("measure specification code").trim());

				// set Action taken details
				ActionTakenModel actionTakenDetails = new ActionTakenModel();
				actionTakenDetails.setActionTaken(rs.getString("action taken").trim());
				actionTakenDetails.setActionTakenDateTime(rs.getString("action taken dateTime"));
				actionTakenDetails.setActionNotes(rs.getString("action note").trim());
				actionTakenDetails.setActionNotesDateTime(rs.getString("action note dateTime"));
				//Multiple action list logic is not added yet.
				List<ActionTakenModel> actionTakenList = new ArrayList();
				actionTakenList.add(actionTakenDetails);
				measuresDetails.setActionTaken(actionTakenList);
				
				measureDetails.add(measuresDetails);
			}

		} catch (SQLException se) {
			
			throw se;
		} finally {
			try {
				if (preparedStatement != null)
					preparedStatement.close();
			} catch (SQLException se) {
				throw se;
			}
			try {
				if (dbConnection != null)
					dbConnection.close();
			} catch (SQLException se) {
				throw se;
			}

		}
		measuresResponse.setStatus("OK");
		measuresResponse.setMeasureDetails(measureDetails);
		
		return measuresResponse;
	}

}
