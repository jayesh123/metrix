/**
 *======================================================================================
 * Response.java
 *======================================================================================
 *
 *	Definitions for class:
 *		- Response
 *
 *======================================================================================
 *	Modification History:
 *======================================================================================
 *
 *  Date:			01/25/2016
 *  				Original development
 *  @author 		Jayesh Bharati
 *  @version 		1.0
 *  Description:  	Response class has a public and private methods. Measures details are set in the response
 *  
 *======================================================================================
 *	Copyright 2016, Sandlot Solutions. All rights reserved.
 *======================================================================================
 **/
package com.sandlotsolutions.MetrixServer.Model;

import java.util.List;

public class Response {
	//PROPERTIES
	private List<MeasuresModel> measureDetails;
	private String status;
	private String errorMessage;
	
	public List<MeasuresModel> getMeasureDetails() {
		return measureDetails;
	}
	public void setMeasureDetails(List<MeasuresModel> measureDetails) {
		this.measureDetails = measureDetails;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	
}
