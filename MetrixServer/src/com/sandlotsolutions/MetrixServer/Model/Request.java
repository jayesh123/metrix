/**
 *======================================================================================
 * Request.java
 *======================================================================================
 *
 *	Definitions for class:
 *		- Request
 *
 *======================================================================================
 *	Modification History:
 *======================================================================================
 *
 *  Date:			02/12/2015
 *  				Original development
 *  @author 		Jayesh Bharati
 *  @version 		1.0
 *  Description:  	Request class has a public and private methods.
 *  
 *======================================================================================
 *	Copyright 2016, Sandlot Solutions. All rights reserved.
 *======================================================================================
 **/

// Package
package com.sandlotsolutions.MetrixServer.Model;

public class Request {
	
	//PROPERTIES
	private String patientId;
	private String patientNameSpaceId;
	private String providerId;
	private String user;
	
	public String getPatientId() {
		return patientId;
	}
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	public String getPatientNameSpaceId() {
		return patientNameSpaceId;
	}
	public void setPatientNameSpaceId(String patientNameSpaceId) {
		this.patientNameSpaceId = patientNameSpaceId;
	}
	public String getProviderId() {
		return providerId;
	}
	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
		
	
}
