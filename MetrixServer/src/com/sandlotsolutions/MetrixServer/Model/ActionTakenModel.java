/**
 *======================================================================================
 * ActionTakenModel.java
 *======================================================================================
 *
 *	Definitions for class:
 *		- ActionTakenModel
 *
 *======================================================================================
 *	Modification History:
 *======================================================================================
 *
 *  Date:			01/25/2016
 *  				Original development
 *  @author 		Jayesh Bharati
 *  @version 		1.0
 *  Description:  	ActionTakenModel is a Bean class.
 *  
 *======================================================================================
 *	Copyright 2016, Sandlot Solutions. All rights reserved.
 *======================================================================================
 **/

// Package
package com.sandlotsolutions.MetrixServer.Model;

import java.util.Date;

public class ActionTakenModel
{
	//PROPERTIES
	private String actionTaken;
	private String actionTakenDateTime;
	private String actionNotes;
	private String actionNotesDateTime;
	
	public String getActionTaken() {
		return actionTaken;
	}
	public void setActionTaken(String actionTaken) {
		this.actionTaken = actionTaken;
	}
	public String getActionTakenDateTime() {
		return actionTakenDateTime;
	}
	public void setActionTakenDateTime(String actionTakenDateTime) {
		this.actionTakenDateTime = actionTakenDateTime;
	}
	public String getActionNotes() {
		return actionNotes;
	}
	public void setActionNotes(String actionNotes) {
		this.actionNotes = actionNotes;
	}
	public String getActionNotesDateTime() {
		return actionNotesDateTime;
	}
	public void setActionNotesDateTime(String actionNotesDateTime) {
		this.actionNotesDateTime = actionNotesDateTime;
	}
	
	
}
