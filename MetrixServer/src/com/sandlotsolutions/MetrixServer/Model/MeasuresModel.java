/**
 *======================================================================================
 * MeasuresModel.java
 *======================================================================================
 *
 *	Definitions for class:
 *		- MeasuresModel
 *
 *======================================================================================
 *	Modification History:
 *======================================================================================
 *
 *  Date:			01/25/2016
 *  				Original development
 *  @author 		Jayesh Bharati
 *  @version 		1.0
 *  Description:  	MeasuresModel is a Bean class.
 *  
 *======================================================================================
 *	Copyright 2016, Sandlot Solutions. All rights reserved.
 *======================================================================================
 **/

// Package
package com.sandlotsolutions.MetrixServer.Model;

// System Imports
import java.util.Date;
import java.util.List;

public class MeasuresModel {
	
	//PROPERTIES
	private String measureName;
	private String measureCode;
	private String measureDescription;
	private String measureSpecificationCode;
	
	// PROPERTY ASSOCIATIONS & AGGREGATIONS
	//private ActionTakenModel actionTaken;
	private List<ActionTakenModel> ActionTaken;

	
	
    public List<ActionTakenModel> getActionTaken() {
		return ActionTaken;
	}
	public void setActionTaken(List<ActionTakenModel> actionTaken) {
		ActionTaken = actionTaken;
	}
	public String getMeasureName() {
		return measureName;
	}
	public void setMeasureName(String measureName) {
		this.measureName = measureName;
	}
	public String getMeasureCode() {
		return measureCode;
	}
	public void setMeasureCode(String measureCode) {
		this.measureCode = measureCode;
	}
	public String getMeasureDescription() {
		return measureDescription;
	}
	public void setMeasureDescription(String measureDescription) {
		this.measureDescription = measureDescription;
	}
	public String getMeasureSpecificationCode() {
		return measureSpecificationCode;
	}
	public void setMeasureSpecificationCode(String measureSpecificationCode) {
		this.measureSpecificationCode = measureSpecificationCode;
	}
	
	
}
