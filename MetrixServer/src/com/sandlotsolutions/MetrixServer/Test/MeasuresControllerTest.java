package com.sandlotsolutions.MetrixServer.Test;

import static org.junit.Assert.*;

import javax.sql.DataSource;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.sandlotsolutions.MetrixServer.DAO.MeasuresDAO;
import com.sandlotsolutions.MetrixServer.Model.Request;
import com.sandlotsolutions.MetrixServer.Util.MeasuresURIEndPoints;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({
	"file:WebContent/WEB-INF/metrixService-servlet.xml"})
@WebAppConfiguration
public class MeasuresControllerTest {
	
	// PROPERTIES
	
	private String patientId="ICW741017";
	private String patientNameSpaceId="3.3.3";
	private String providerId="3";

	@Autowired
	private WebApplicationContext wac;
	
	@Autowired
	public MeasuresDAO measuresDAO;
	private MockMvc mockMvc;
	
	@Before
	public void setUp() throws Exception {
		
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
		
	}

	 @BeforeClass
     public static void setupProperties() {
		 System.setProperty("jdbc.driverClassName", "com.microsoft.sqlserver.jdbc.SQLServerDriver");
		 System.setProperty("jdbc.url", "jdbc:sqlserver://SNDFWTDBTW2K801\\SNDDEV2;databaseName=QRM_NTSP_KMS_QC;");
		 System.setProperty("jdbc.username", "MetrixUser1");
		 System.setProperty("jdbc.password", "MetrixUser1");
     }

	
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testGetMeasurements() throws Exception {
		
		Request request = new Request();
		request.setPatientId(patientId);
		request.setPatientNameSpaceId(patientNameSpaceId);
		request.setProviderId(providerId);
		MvcResult result = this.mockMvc.perform(MockMvcRequestBuilders.post(MeasuresURIEndPoints.GET_MEASURES)
				.content(new ObjectMapper().writeValueAsString(request))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andReturn();
		
		System.out.println(result.getResponse().getContentAsString());

		MockHttpServletResponse httpRespone = result.getResponse();
		com.sandlotsolutions.MetrixServer.Model.Response response=new ObjectMapper().readValue(httpRespone.getContentAsString(), com.sandlotsolutions.MetrixServer.Model.Response.class);
		assertEquals("OK", response.getStatus());
	}

}
