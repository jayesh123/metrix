/**
 *======================================================================================
 * SQLQueryConstants.java
 *======================================================================================
 *
 *	Definitions for class:
 *		- SQLQueryConstants
 *
 *======================================================================================
 *	Modification History:
 *======================================================================================
 *
 *  Date:			01/25/2016
 *  				Original development
 *  @author 		Jayesh Bharati
 *  @version 		1.0
 *  Description:  	SQLQueryConstants is a class to define SQL queries. 
 *  
 *======================================================================================
 *	Copyright 2016, Sandlot Solutions. All rights reserved.
 *======================================================================================
 **/
package com.sandlotsolutions.MetrixServer.Util;

public class SQLQueryConstants {

	public static final String GET_MEASURES_SQL="select "+
			"QMM.measure_name                    as 'measure name' "+
			",QAT.action_taken_name              as 'action taken' "+
			",QPAC.Date_Completed                as 'action taken dateTime' "+
			",QMM.measure_code                   as 'measure code' "+
			",QMM.measure_description            as 'measure description' "+
			",QMS.measure_spec_name              as 'measure specification code' "+
			",QPAC.Action_Taken_Other_Value      as 'action note' "+
			",QPAC.Created_Datetime              as 'action note dateTime' "+
			"from [QRM_NTSP_KMS_QC].[dbo].QRM_Population_Action_Completion QPAC "+
			"inner join [QRM_NTSP_KMS_QC].[dbo].QRM_Measure_Master QMM on QPAC.measure_master_id = QMM.measure_master_id "+
			"inner join [QRM_NTSP_KMS_QC].[dbo].QRM_Action_Taken  QAT on QPAC.action_taken_id = QAT.action_taken_id "+
			"inner join [QRM_NTSP_KMS_QC].[dbo].QRM_Measure_Specification    QMS ON QMM.measure_spec_id = QMS.measure_spec_id "+
			" where QPAC.Patient_ID=?";
}
