/**
 *======================================================================================
 * MeasuresURIEndPoints.java
 *======================================================================================
 *
 *	Definitions for class:
 *		- MeasuresURIEndPoints
 *
 *======================================================================================
 *	Modification History:
 *======================================================================================
 *
 *  Date:			02/12/2015
 *  				Original development
 *  @author 		Jayesh Bharati
 *  @version 		1.0
 *  Description:  	MeasuresURIEndPoints contains the mapping URL constants
 *  				Design Pattern(s):
 *  				 - None
 *  
 *======================================================================================
 *	Copyright 2016, Sandlot Solutions. All rights reserved.
 *======================================================================================
 **/
package com.sandlotsolutions.MetrixServer.Util;

/**
 * @author jayeshb
 *
 */
public class MeasuresURIEndPoints {
	
	// STATIC DEFINITIONS
	public static final String ROOT = "/";
	public static final String GET_MEASURES = "measures/getMeasures";

}
