/**
 *======================================================================================
 * CommonUtil.java
 *======================================================================================
 *
 *	Definitions for class:
 *		- CommonUtil
 *
 *======================================================================================
 *	Modification History:
 *======================================================================================
 *
 *  Date:			01/25/2016
 *  				Original development
 *  @author 		Jayesh Bharati
 *  @version 		1.0
 *  Description:  	CommonUtil is a class to define common public methods. 
 *  
 *======================================================================================
 *	Copyright 2016, Sandlot Solutions. All rights reserved.
 *======================================================================================
 **/
package com.sandlotsolutions.MetrixServer.Util;

import java.text.SimpleDateFormat;

public class CommonUtil {
	
	//===================================================================
	// PUBLIC STATIC FUNCTIONS
	//===================================================================
	/** 
	  *
	  * This method validates NULL and Empty check of parameters.
	  * 
	  * @param params      it takes one or more parameters as input
	  * 
	  * @return boolean 	 <code>true</code> if found NULL or empty 
	  * 
	  **/
	public static boolean verifyNullOrEmpty(String... params){
		int paramsLength=params.length;
		boolean checkFlag=false;
		for(String param:params){
			if(param==null ){
				return true;
			}
			if(param.trim().length()==0){
				return true;
			}
		}
		return checkFlag;
		
	}

}
